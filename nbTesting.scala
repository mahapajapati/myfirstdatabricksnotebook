// Databricks notebook source
// Obtain parameters values from the ADF pipeline

dbutils.widgets.text("storageAccountName", "storageaccounttrainingls","Storage Account Name")
dbutils.widgets.text("inputBlobContainer", "containertraining","Input Blob Container")
dbutils.widgets.text("inputFolderPath", "csv_files","Input Folder Path")
dbutils.widgets.text("inputFileName", "cars.csv","Input File Name")

val storageAccountName = dbutils.widgets.get("storageAccountName")
val storageAccountKey = "j3NPsOVKqU7kh/ng9eYMfO33FjEGQUTmn3eM59hEVOoKgkgxwAdyM99udLOJPqT/a89+kmt9xjg0fvpJorvC4A=="
val inputBlobContainer = dbutils.widgets.get("inputBlobContainer")
val inputFolderPath = dbutils.widgets.get("inputFolderPath")
val inputFileName = dbutils.widgets.get("inputFileName")


// COMMAND ----------

import scala.io.Source
import org.apache.hadoop.fs.FileSystem
import org.apache.hadoop.fs.Path
import org.apache.hadoop.io.IOUtils

//Creating the filePath and fileName values
// Using an Storage Account key to connect by Hadoop Configuration
spark.sparkContext.hadoopConfiguration.set(s"fs.azure.account.key.$storageAccountName.blob.core.windows.net", storageAccountKey)

// Configure the input full path to read the file in the specific directory
var basePath = s"wasbs://${inputBlobContainer}@${storageAccountName}.blob.core.windows.net/"
val filePath = s"${basePath}${inputFolderPath}/${inputFileName}"

// List the content of `inputFolderPath` (csv_files)
dbutils.fs.ls(basePath+"csv_files")


// COMMAND ----------

import au.com.bytecode.opencsv.CSVReader
import java.io.IOException
import java.io.Reader
import java.nio.file.Files
import java.nio.file.Paths
import java.util.List;

val fsFileCopy = s"/newCsvs"

// Create a directory in dbfs
val isNewFolderCreated = dbutils.fs.mkdirs(fsFileCopy)
println("---a----"+isNewFolderCreated)

// Copy the filePath (storage account path) in the new directory
val isCsvFileCopied = dbutils.fs.cp(filePath, s"${fsFileCopy}/", true)
println("---b----"+isCsvFileCopied)

// Define the new path (include dbfs reference)
val newFilePath = s"/dbfs${fsFileCopy}/${inputFileName}"
println("---c----"+newFilePath)

// Get the file from the new path in dbfs
val fileInput = Paths.get(newFilePath)
println("---1----"+fileInput)

// Read the file
val reader = Files.newBufferedReader(fileInput)
println("----2---"+reader)

// Read the file as csv using CsvReader
val csvReader = new CSVReader(reader)
println("----3---"+csvReader)

//Read all rows at once
val allRows: List[Array[String]] = csvReader.readAll();


// COMMAND ----------

import java.util.Arrays;
//import scala.collection.JavaConversions._
import scala.jdk.CollectionConverters._

// Convert the java list to a scala seq
val scalaSeq: Seq[Array[String]] = allRows.asScala.toSeq
// Convert the fourth column to upperCase
for(x <- scalaSeq.slice(1, scalaSeq.length-1)) {
  //println(x(3))
  x(3) = x(3).toUpperCase()
}


// COMMAND ----------

import au.com.bytecode.opencsv.CSVWriter
import java.io.FileWriter

// Create a new File to write the updated content
val outputFile = new FileWriter(s"/dbfs${fsFileCopy}/output.csv")
// Create a csv file using `outputFile`
val writer = new CSVWriter(outputFile,',')
// Write all the content from previous updated csv to the new csv
writer.writeAll(allRows)
// Actually writes the csv
writer.flush()
// Flushes the file again
writer.close()

// COMMAND ----------

// Read the written output file and check the updated column
val fileInput2 = Paths.get(s"/dbfs${fsFileCopy}/output.csv")
println("---1----"+fileInput2)

val reader2 = Files.newBufferedReader(fileInput2)
println("----2---"+reader2)

val csvReader2 = new CSVReader(reader2)
println("----3---"+csvReader2)

// Read all rows at once
val allRows2: List[Array[String]] = csvReader2.readAll();
// Convert the java list to a scala seq
val scalaSeq2: Seq[Array[String]] = allRows2.asScala.toSeq

for(x <- scalaSeq2) {
  println(x(3))
}

// COMMAND ----------

// Save the output file in the storage account container
dbutils.fs.cp(s"${fsFileCopy}/output.csv", basePath+"output.csv", true)

// COMMAND ----------

dbutils.fs.rm(fsFileCopy, true)

// COMMAND ----------


